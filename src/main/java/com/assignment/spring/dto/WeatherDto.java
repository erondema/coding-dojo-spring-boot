package com.assignment.spring.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * A data transfer object containing weather information for a specific city.
 *
 * @author Erik Rondema
 */
@Getter
@Setter
@NoArgsConstructor
public class WeatherDto {

    private Integer id;
    private String city;
    private String country;
    private Double temperature;
}
