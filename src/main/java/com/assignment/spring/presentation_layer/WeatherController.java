package com.assignment.spring.presentation_layer;

import com.assignment.spring.dto.WeatherDto;
import com.assignment.spring.service_layer.WeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * This REST controller is responsible for providing an endpoint to retrieve weather information of a city.
 *
 * @author Erik Rondema
 */
@RestController
public class WeatherController {

    private WeatherService weatherService;

    @Autowired
    public void setWeatherService(WeatherService weatherService) {
        this.weatherService = weatherService;
    }

    @RequestMapping("/weather")
    public WeatherDto getWeatherForCity(@RequestParam String city) {
        return weatherService.findWeatherByCity(city);
    }
}
