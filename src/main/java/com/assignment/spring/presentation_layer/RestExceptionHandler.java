package com.assignment.spring.presentation_layer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.RestClientResponseException;

/**
 * This handler is responsible for converting exceptions into an {@link ResponseEntity} with the correct
 * HTTP status and a message for the caller.
 *
 * @author Erik Rondema
 */
@RestControllerAdvice
public class RestExceptionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(RestExceptionHandler.class);

    @ExceptionHandler(RestClientResponseException.class)
    public ResponseEntity<String> handleOpenWeatherServiceExpections(RestClientResponseException rcre) {

        LOG.error("Problem with the OpenWeather service", rcre);
        int responseCode = rcre.getRawStatusCode();
        switch (responseCode) {
            case 401: return new ResponseEntity<>("Access to the OpenWeather service denied",
                    HttpStatus.UNAUTHORIZED);
            case 404: return new ResponseEntity<>("The city is unknown to this service", HttpStatus.NOT_FOUND);
            case 500: return new ResponseEntity<>("The OpenWeather service returned with an server error",
                    HttpStatus.SERVICE_UNAVAILABLE);
            default: return new ResponseEntity<>("An unexpected error occured", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ExceptionHandler(DataAccessException.class)
    public ResponseEntity<String> handleDatabaseExpections(DataAccessException dae) {

        LOG.error("Problem with the database", dae);
        return new ResponseEntity<>("There is a problem with the database", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<String> handleRuntimeExpections(RuntimeException re) {

        LOG.error("Unexpected expection", re);
        return new ResponseEntity<>("There is a problem with the service", HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
