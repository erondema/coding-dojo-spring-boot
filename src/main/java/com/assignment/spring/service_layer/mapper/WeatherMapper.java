package com.assignment.spring.service_layer.mapper;

import com.assignment.spring.data_layer.openweathermap.domain.WeatherResponse;
import com.assignment.spring.dto.WeatherDto;
import com.assignment.spring.entity.WeatherEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * This mapper is responsible for converting the following objects:
 * <ul>
 *     <li>{@link WeatherEntity} into {@link WeatherDto}</li>
 *     <li>{@link WeatherResponse} into {@link WeatherEntity}</li>
 * </ul>
 *
 * MapStruct will generate an implementation for this interface during compilation.
 *
 * @author Erik Rondema
 */
@Mapper
public interface WeatherMapper {

    WeatherDto entityToDto(WeatherEntity entity);

    @Mapping(target = "country", source = "sys.country")
    @Mapping(target = "city", source = "name")
    @Mapping(target = "temperature", source = "main.temp")
    WeatherEntity weatherResponseToEntity(WeatherResponse weatherResponse);
}
