package com.assignment.spring.service_layer;

import com.assignment.spring.dto.WeatherDto;

/**
 * The weather service is responsible for all business logic related to weather information.
 *
 * @author Erik Rondema
 */
public interface WeatherService {

    /**
     * Find the weather information for the given city and store this information in the database.
     *
     * @param city the name of the city we want weather information for
     * @return a DTO containing weather information
     */
    WeatherDto findWeatherByCity(String city);
}
