package com.assignment.spring.service_layer;

import com.assignment.spring.data_layer.WeatherRepository;
import com.assignment.spring.data_layer.openweathermap.OpenWeatherMapRepository;
import com.assignment.spring.service_layer.mapper.WeatherMapper;
import org.mapstruct.factory.Mappers;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * This class is responsible for creating all Spring beans in the service layer.
 *
 * @author Erik Rondema
 */
@Configuration
public class ServiceLayerConfig {

    @Bean
    public WeatherService weatherService(WeatherRepository weatherRepository,
                                         OpenWeatherMapRepository openWeatherMapRepository,
                                         WeatherMapper mapper) {
        return new WeatherServiceImpl(weatherRepository, openWeatherMapRepository, mapper);
    }

    @Bean
    public WeatherMapper weatherMapper() {
        return Mappers.getMapper(WeatherMapper.class);
    }
}
