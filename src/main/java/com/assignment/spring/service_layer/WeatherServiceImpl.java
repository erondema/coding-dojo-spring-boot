package com.assignment.spring.service_layer;

import com.assignment.spring.data_layer.WeatherRepository;
import com.assignment.spring.data_layer.openweathermap.OpenWeatherMapRepository;
import com.assignment.spring.data_layer.openweathermap.domain.WeatherResponse;
import com.assignment.spring.dto.WeatherDto;
import com.assignment.spring.entity.WeatherEntity;
import com.assignment.spring.service_layer.mapper.WeatherMapper;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of the {@link WeatherService}.
 *
 * @author Erik Rondema
 */
@Transactional
public class WeatherServiceImpl implements WeatherService {

    private WeatherRepository weatherRepository;
    private OpenWeatherMapRepository openWeatherMapRepository;
    private WeatherMapper mapper;

    /**
     * Constructor.
     */
    public WeatherServiceImpl(WeatherRepository weatherRepository,
                              OpenWeatherMapRepository openWeatherMapRepository,
                              WeatherMapper mapper) {
        this.weatherRepository = weatherRepository;
        this.openWeatherMapRepository = openWeatherMapRepository;
        this.mapper = mapper;
    }

    @Override
    public WeatherDto findWeatherByCity(String city) {

        WeatherResponse response = openWeatherMapRepository.findByCity(city);
        WeatherEntity weatherEntity = mapper.weatherResponseToEntity(response);
        weatherEntity = weatherRepository.save(weatherEntity);
        return mapper.entityToDto(weatherEntity);
    }
}
