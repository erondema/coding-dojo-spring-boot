package com.assignment.spring.data_layer;

import com.assignment.spring.data_layer.openweathermap.OpenWeatherMapProperties;
import com.assignment.spring.data_layer.openweathermap.OpenWeatherMapRepository;
import com.assignment.spring.data_layer.openweathermap.RestOpenWeatherMapRepository;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.client.RestTemplate;

import javax.sql.DataSource;

/**
 * This class is responsible for creating all Spring beans in the data layer.
 *
 * @author Erik Rondema
 */
@Configuration
@EnableJpaRepositories
@EnableTransactionManagement
@EntityScan(basePackages = { "com.assignment.spring.entity" })
public class DataLayerConfig {

    @Bean
    public OpenWeatherMapRepository openWeatherMapRepository(RestTemplate restTemplate,
                                                             OpenWeatherMapProperties properties) {
        return new RestOpenWeatherMapRepository(restTemplate, properties.getUrl(), properties.getApiKey());
    }

    @Bean
    public DataSource dataSource(DatabaseProperties properties) {

        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setJdbcUrl(properties.getDatabaseUrl());
        dataSource.setDriverClassName(properties.getDriverClassName());
        dataSource.setUsername(properties.getDatabaseUsername());
        dataSource.setPassword(properties.getDatabasePassword());
        return dataSource;
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
