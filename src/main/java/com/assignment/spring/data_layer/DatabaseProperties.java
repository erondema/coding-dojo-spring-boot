package com.assignment.spring.data_layer;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * This class contains the properies that start with "db" and configure the database connection.
 *
 * @author Erik Rondema
 */
@Configuration
@ConfigurationProperties(value = "db")
@Getter
@Setter
@NoArgsConstructor
public class DatabaseProperties {

    private String driverClassName;
    private String databaseUrl;
    private String databaseUsername;
    private String databasePassword;
}
