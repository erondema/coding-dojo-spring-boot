package com.assignment.spring.data_layer.openweathermap;

import com.assignment.spring.data_layer.openweathermap.domain.WeatherResponse;

/**
 * This interface defines the information that can be retrieved from the OpenWeatherMap API.
 *
 * @author Erik Rondema
 */
public interface OpenWeatherMapRepository {

    /**
     * Retrieve weather information for the given city.
     *
     * @param city the name of the city we want weather information for
     * @return a response containing weather information
     */
    WeatherResponse findByCity(String city);
}
