package com.assignment.spring.data_layer.openweathermap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * This class contains the properies that start with "openweathermap" and configure the connection with the
 * OpenWeatherMap REST API.
 *
 * @author Erik Rondema
 */
@Configuration
@ConfigurationProperties(value = "openweathermap")
@Getter
@Setter
@NoArgsConstructor
public class OpenWeatherMapProperties {

    String url;
    String apiKey;
}
