package com.assignment.spring.data_layer.openweathermap;

import com.assignment.spring.data_layer.openweathermap.domain.WeatherResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 * This class implements the {@link OpenWeatherMapRepository} by calling the REST API of the OpenWeatherMap service.
 *
 * @author Erik Rondema
 */
public class RestOpenWeatherMapRepository implements OpenWeatherMapRepository {

    private static final String BY_NAME_URL = "/weather?q={city}&APPID={appid}";

    private RestTemplate restTemplate;
    private String openWeatherUrl;
    private String apiKey;

    /**
     * Constructor.
     *
     * @param restTemplate a Spring RestTemplate
     * @param openWeatherUrl the URL to the OpenWeather service
     * @param apiKey the API key of the OpenWeather service that is receive upon registration
     */
    public RestOpenWeatherMapRepository(RestTemplate restTemplate, String openWeatherUrl, String apiKey) {
        this.restTemplate = restTemplate;
        this.openWeatherUrl = openWeatherUrl;
        this.apiKey = apiKey;
    }

    @Override
    public WeatherResponse findByCity(String city) {

        String url = openWeatherUrl + BY_NAME_URL;
        url = url.replace("{city}", city);
        url = url.replace("{appid}", apiKey);

        ResponseEntity<WeatherResponse> response = restTemplate.getForEntity(url, WeatherResponse.class);
        return response.getBody();
    }
}
