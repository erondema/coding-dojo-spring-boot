package com.assignment.spring.data_layer;

import com.assignment.spring.entity.WeatherEntity;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DbConfig.class})
@ActiveProfiles("TEST")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:test_schema.sql")
public class WeatherRepositoryTest {

    @Autowired
    private WeatherRepository objectUnderTest;

    @Autowired
    private DataSource dataSource;

    private JdbcTemplate jdbcTemplate;

    @BeforeClass
    public static void startDatabase() {
        DbConfig.startContainer();
    }

    @AfterClass
    public static void stopDatabase() {
        DbConfig.stopContainer();
    }

    @Before
    public void setup() {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Test
    public void testSave() {

        WeatherEntity weatherEntity = new WeatherEntity();
        weatherEntity.setCity("Amsterdam");
        weatherEntity.setCountry("The Netherlands");
        weatherEntity.setTemperature(Double.valueOf("22.60"));

        WeatherEntity savedWeatherEntity = objectUnderTest.save(weatherEntity);
        Integer id = savedWeatherEntity.getId();

        assertNotNull(savedWeatherEntity);
        assertNotNull(id);

        Map<String, Object> resultMap = jdbcTemplate.queryForMap("SELECT * FROM dojo.weather WHERE id=" + id);
        assertEquals(weatherEntity.getCity(), resultMap.get("city"));
        assertEquals(weatherEntity.getCountry(), resultMap.get("country"));

        BigDecimal temperature = (BigDecimal) resultMap.get("temperature");
        assertEquals(weatherEntity.getTemperature(), temperature.doubleValue(), 0.0);
    }
}