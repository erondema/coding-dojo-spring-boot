package com.assignment.spring.data_layer;

import org.hibernate.cfg.AvailableSettings;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.testcontainers.containers.PostgreSQLContainer;

import javax.sql.DataSource;
import java.time.Duration;
import java.util.Properties;

import static java.lang.String.format;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackageClasses = { WeatherRepository.class })
@Profile("TEST")
public class DbConfig {

    private static final String SCHEMA_NAME = "dojo";
    private static PostgreSQLContainer postgreSQLContainer;

    static void startContainer() {

        postgreSQLContainer = (PostgreSQLContainer)
                new PostgreSQLContainer("postgres:10.6")
                        .withDatabaseName("test")
                        .withUsername(SCHEMA_NAME)
                        .withPassword("g3h31m").withStartupTimeout(Duration.ofSeconds(600));

        postgreSQLContainer.start();
    }

    static void stopContainer() {
        postgreSQLContainer.stop();
    }

    @Bean
    public DataSource dataSource() {

        String ipAddress = postgreSQLContainer.getContainerIpAddress();
        Integer port = postgreSQLContainer.getMappedPort(PostgreSQLContainer.POSTGRESQL_PORT);
        String databaseName = postgreSQLContainer.getDatabaseName();

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl(format("jdbc:postgresql://%s:%s/%s", ipAddress, port, databaseName));
        dataSource.setUsername(postgreSQLContainer.getUsername());
        dataSource.setPassword(postgreSQLContainer.getPassword());
        dataSource.setSchema(postgreSQLContainer.getDatabaseName());
        return dataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) {

        LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setPackagesToScan("com.assignment.spring.entity");
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setJpaProperties(getHibernateProperties());
        factoryBean.afterPropertiesSet();
        return factoryBean;
    }

    @Bean
    public JpaTransactionManager transactionManager(LocalContainerEntityManagerFactoryBean factoryBean) {

        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(factoryBean.getObject());
        return transactionManager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    private Properties getHibernateProperties() {

        Properties properties = new Properties();
        properties.put("hibernate.temp.use_jdbc_metadata_defaults", "false");
        properties.put("hibernate.dialect", "org.hibernate.dialect.PostgreSQL95Dialect");
        properties.put("hibernate.hbm2ddl.auto", "none");
        properties.put("hibernate.connection.characterEncoding", "UTF-8");
        properties.put("hibernate.connection.charSet", "UTF-8");
        properties.put("hibernate.default_schema", SCHEMA_NAME);

        properties.put(AvailableSettings.FORMAT_SQL, "true");
        properties.put(AvailableSettings.SHOW_SQL, "true");
        return properties;
    }
}