package com.assignment.spring.data_layer.openweathermap;

import com.assignment.spring.TestUtil;
import com.assignment.spring.data_layer.DataLayerConfig;
import com.assignment.spring.data_layer.openweathermap.domain.WeatherResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RestOpenWeatherMapRepositoryTest {

    private static final String URL = "http://api.openweathermap.org/data/2.5";
    private static final String API_KEY = "1243123523645541adsfads";

    private RestOpenWeatherMapRepository objectUnderTest;

    @Captor
    ArgumentCaptor<String> urlCaptor;

    @Captor
    ArgumentCaptor<Class<Object>> classCaptor;

    @Mock
    private RestTemplate restTemplate;

    @Before
    public void setup() {

        DataLayerConfig config = new DataLayerConfig();
        OpenWeatherMapProperties properties = new OpenWeatherMapProperties();
        properties.setUrl(URL);
        properties.setApiKey(API_KEY);
        objectUnderTest = (RestOpenWeatherMapRepository) config.openWeatherMapRepository(restTemplate, properties);
    }

    @Test
    public void testFindByCity() {

        String country = "NL";
        String city = "Amsterdam";
        Double temperature = Double.valueOf("22.35");

        WeatherResponse weatherResponse = TestUtil.createWeatherResponse(country, city, temperature);
        ResponseEntity<Object> responseEntity = new ResponseEntity<>(weatherResponse, HttpStatus.OK);
        when(restTemplate.getForEntity(urlCaptor.capture(), classCaptor.capture())).thenReturn(responseEntity);

        WeatherResponse response = objectUnderTest.findByCity("Amsterdam");

        assertEquals(weatherResponse, response);
        assertEquals(URL + "/weather?q=" + city + "&APPID=" + API_KEY, urlCaptor.getValue());
        assertEquals(WeatherResponse.class, classCaptor.getValue());
    }
}