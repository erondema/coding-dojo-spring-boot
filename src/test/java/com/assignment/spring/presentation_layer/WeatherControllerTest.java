package com.assignment.spring.presentation_layer;

import com.assignment.spring.TestUtil;
import com.assignment.spring.dto.WeatherDto;
import com.assignment.spring.service_layer.WeatherService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.RecoverableDataAccessException;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.client.RestClientResponseException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = WeatherController.class)
public class WeatherControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private WeatherService weatherService;

    private ObjectMapper objectMapper;

    @Before
    public void setup() {
        objectMapper = new ObjectMapper();
    }

    @Test
    public void testGetWeatherForCity() throws Exception {

        Integer id = 45;
        String country = "NL";
        String city = "Amsterdam";
        Double temperature = Double.valueOf("14.66");

        WeatherDto weatherDto = TestUtil.createWeatherDto(id, country, city, temperature);
        when(weatherService.findWeatherByCity(city)).thenReturn(weatherDto);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/weather");
        builder.param("city", city);
        MvcResult result = mockMvc.perform(builder).andReturn();

        assertNotNull(result);
        MockHttpServletResponse response = result.getResponse();
        assertEquals(200, response.getStatus());

        WeatherDto resultDto = objectMapper.readValue(response.getContentAsString(), WeatherDto.class);
        assertEquals(id, resultDto.getId());
        assertEquals(country, resultDto.getCountry());
        assertEquals(city, resultDto.getCity());
        assertEquals(temperature, resultDto.getTemperature());
    }

    @Test
    public void testGetWeatherForCityAccessDenied() throws Exception {

        String city = "Amsterdam";
        RestClientResponseException exception = new RestClientResponseException("", 401, null, null, null, null);
        when(weatherService.findWeatherByCity(city)).thenThrow(exception);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/weather");
        builder.param("city", city);
        MvcResult result = mockMvc.perform(builder).andReturn();

        assertNotNull(result);
        MockHttpServletResponse response = result.getResponse();
        assertEquals(401, response.getStatus());
        assertEquals("Access to the OpenWeather service denied", response.getContentAsString());
    }

    @Test
    public void testGetWeatherForCityUnknownCity() throws Exception {

        String city = "Bla";
        RestClientResponseException exception = new RestClientResponseException("", 404, null, null, null, null);
        when(weatherService.findWeatherByCity(city)).thenThrow(exception);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/weather");
        builder.param("city", city);
        MvcResult result = mockMvc.perform(builder).andReturn();

        assertNotNull(result);
        MockHttpServletResponse response = result.getResponse();
        assertEquals(404, response.getStatus());
        assertEquals("The city is unknown to this service", response.getContentAsString());
    }

    @Test
    public void testGetWeatherForCityOpenWeatherError() throws Exception {

        String city = "Amsterdam";
        RestClientResponseException exception = new RestClientResponseException("", 500, null, null, null, null);
        when(weatherService.findWeatherByCity(city)).thenThrow(exception);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/weather");
        builder.param("city", city);
        MvcResult result = mockMvc.perform(builder).andReturn();

        assertNotNull(result);
        MockHttpServletResponse response = result.getResponse();
        assertEquals(503, response.getStatus());
        assertEquals("The OpenWeather service returned with an server error", response.getContentAsString());
    }

    @Test
    public void testGetWeatherForCityOpenWeatherUnexpectedError() throws Exception {

        String city = "Amsterdam";
        RestClientResponseException exception = new RestClientResponseException("", 501, null, null, null, null);
        when(weatherService.findWeatherByCity(city)).thenThrow(exception);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/weather");
        builder.param("city", city);
        MvcResult result = mockMvc.perform(builder).andReturn();

        assertNotNull(result);
        MockHttpServletResponse response = result.getResponse();
        assertEquals(500, response.getStatus());
        assertEquals("An unexpected error occured", response.getContentAsString());
    }

    @Test
    public void testGetWeatherForCityDatabaseError() throws Exception {

        String city = "Amsterdam";
        RecoverableDataAccessException exception = new RecoverableDataAccessException("bla bla");
        when(weatherService.findWeatherByCity(city)).thenThrow(exception);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/weather");
        builder.param("city", city);
        MvcResult result = mockMvc.perform(builder).andReturn();

        assertNotNull(result);
        MockHttpServletResponse response = result.getResponse();
        assertEquals(500, response.getStatus());
        assertEquals("There is a problem with the database", response.getContentAsString());
    }

    @Test
    public void testGetWeatherForCityServiceError() throws Exception {

        String city = "Amsterdam";
        NullPointerException exception = new NullPointerException("bla bla");
        when(weatherService.findWeatherByCity(city)).thenThrow(exception);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/weather");
        builder.param("city", city);
        MvcResult result = mockMvc.perform(builder).andReturn();

        assertNotNull(result);
        MockHttpServletResponse response = result.getResponse();
        assertEquals(500, response.getStatus());
        assertEquals("There is a problem with the service", response.getContentAsString());
    }
}