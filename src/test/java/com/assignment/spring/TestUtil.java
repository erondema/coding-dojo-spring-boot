package com.assignment.spring;

import com.assignment.spring.data_layer.openweathermap.domain.Main;
import com.assignment.spring.data_layer.openweathermap.domain.Sys;
import com.assignment.spring.data_layer.openweathermap.domain.WeatherResponse;
import com.assignment.spring.dto.WeatherDto;
import com.assignment.spring.entity.WeatherEntity;

public class TestUtil {

    public static WeatherResponse createWeatherResponse(String country, String city, Double temperature) {

        WeatherResponse weatherResponse = new WeatherResponse();
        weatherResponse.setName(city);

        Sys sys = new Sys();
        sys.setCountry(country);
        weatherResponse.setSys(sys);

        Main main = new Main();
        main.setTemp(temperature);
        weatherResponse.setMain(main);

        return weatherResponse;
    }

    public static WeatherEntity createWeatherEntity(Integer id, String country, String city, Double temperature) {

        WeatherEntity weatherEntity = new WeatherEntity();
        weatherEntity.setId(id);
        weatherEntity.setCountry(country);
        weatherEntity.setCity(city);
        weatherEntity.setTemperature(temperature);
        return weatherEntity;
    }

    public static WeatherDto createWeatherDto(Integer id, String country, String city, Double temperature) {

        WeatherDto weatherDto = new WeatherDto();
        weatherDto.setId(id);
        weatherDto.setCountry(country);
        weatherDto.setCity(city);
        weatherDto.setTemperature(temperature);
        return weatherDto;
    }
}
