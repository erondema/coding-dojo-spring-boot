package com.assignment.spring.service_layer;

import com.assignment.spring.TestUtil;
import com.assignment.spring.data_layer.WeatherRepository;
import com.assignment.spring.data_layer.openweathermap.OpenWeatherMapRepository;
import com.assignment.spring.data_layer.openweathermap.domain.WeatherResponse;
import com.assignment.spring.dto.WeatherDto;
import com.assignment.spring.entity.WeatherEntity;
import com.assignment.spring.service_layer.mapper.WeatherMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class WeatherServiceImplTest {

    private WeatherServiceImpl objectUnderTest;

    @Mock
    private WeatherRepository weatherRepository;

    @Mock
    private OpenWeatherMapRepository openWeatherMapRepository;

    @Mock
    private WeatherMapper mapper;

    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);

        ServiceLayerConfig config = new ServiceLayerConfig();
        objectUnderTest = (WeatherServiceImpl) config.weatherService(weatherRepository, openWeatherMapRepository, mapper);
    }

    @Test
    public void testFindWeatherByCity() {

        Integer id = 45;
        String country = "NL";
        String city = "Amsterdam";
        Double temperature = Double.valueOf("22.35");

        WeatherResponse weatherResponse = TestUtil.createWeatherResponse(country,city, temperature);
        WeatherEntity weatherEntity = TestUtil.createWeatherEntity(id, country, city, temperature);
        WeatherDto weatherDto = TestUtil.createWeatherDto(id, country, city, temperature);

        when(openWeatherMapRepository.findByCity(city)).thenReturn(weatherResponse);
        when(weatherRepository.save(any(WeatherEntity.class))).thenReturn(weatherEntity);
        when(mapper.weatherResponseToEntity(weatherResponse)).thenReturn(weatherEntity);
        when(mapper.entityToDto(weatherEntity)).thenReturn(weatherDto);


        WeatherDto result = objectUnderTest.findWeatherByCity(city);

        assertEquals(weatherDto.getId(), result.getId());
        assertEquals(weatherDto.getCountry(), result.getCountry());
        assertEquals(weatherDto.getCity(), result.getCity());
        assertEquals(weatherDto.getTemperature(), result.getTemperature());

        verify(weatherRepository, times(1)).save(weatherEntity);
    }
}