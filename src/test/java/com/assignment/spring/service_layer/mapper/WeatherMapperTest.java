package com.assignment.spring.service_layer.mapper;

import com.assignment.spring.data_layer.openweathermap.domain.WeatherResponse;
import com.assignment.spring.dto.WeatherDto;
import com.assignment.spring.entity.WeatherEntity;
import com.assignment.spring.service_layer.ServiceLayerConfig;
import com.assignment.spring.TestUtil;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class WeatherMapperTest {

    private WeatherMapper objectUnderTest;

    @Before
    public void setup() {

        ServiceLayerConfig config = new ServiceLayerConfig();
        objectUnderTest = config.weatherMapper();
    }

    @Test
    public void testEntityToDto() {

        Integer id = 45;
        String country = "NL";
        String city = "Amsterdam";
        Double temperature = Double.valueOf("22.35");
        WeatherEntity entity = TestUtil.createWeatherEntity(id, country, city, temperature);

        WeatherDto dto = objectUnderTest.entityToDto(entity);

        assertEquals(id, dto.getId());
        assertEquals(country, dto.getCountry());
        assertEquals(city, dto.getCity());
        assertEquals(temperature, dto.getTemperature(), 0.0);
    }

    @Test
    public void testWeatherResponseToEntity() {

        String country = "NL";
        String city = "Amsterdam";
        Double temperature = Double.valueOf("22.35");
        WeatherResponse weatherResponse = TestUtil.createWeatherResponse(country, city, temperature);

        WeatherEntity entity = objectUnderTest.weatherResponseToEntity(weatherResponse);

        assertNull(entity.getId());
        assertEquals(country, entity.getCountry());
        assertEquals(city, entity.getCity());
        assertEquals(temperature, entity.getTemperature(), 0.0);
    }
}