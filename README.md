# Spring Boot Coding Dojo

Welcome to the Spring Boot Coding Dojo!

## OpenWeatherMap
This application will request the weather a [OpenWeatherMap](https://openweathermap.org/).
To run this application you need an API key which you will receive when you register at
OpenWeatherMap.

## IDE setup
This project uses Maven (https://maven.apache.org/index.html) to build the software, so instruct your IDE to 
use maven to configure your project. <br>
Because this project uses the Lombok project (https://projectlombok.org/) you need to configure your IDE to 
support this project, otherwise your IDE will get compilation errors. 
See https://projectlombok.org/setup/overview for instructions for your IDE and additional information for 
Eclipse and Intellij at https://www.baeldung.com/lombok-ide.
<br>
This project also uses MapStruct (http://mapstruct.org/) and you can configure IDE support for this project 
as well with instructions at http://mapstruct.org/documentation/ide-support/.

## Docker
Some of the JUnit tests will start a Docker container to run tests against a Postgres database. Therefore 
Docker Desktop must be installed on your computer. See https://docs.docker.com/desktop/ for instructions to
install Docker Desktop.

## Database
Before running the application a Postgres database should be available. It is easy to start a Postgres database
inside a Docker container with the following command.
```
docker run --name dojo-postgres -e POSTGRES_PASSWORD=<db-password> -d postgres
```
Replace `<db-password>` with a password of your choice.


After Postgres has started execute the SQL statements from the `schema.sql` file that is
located in the `src/main/resources/data` directory.

## Running the application
Before you can run the application you need to build the application. This can be done 
with the following maven command in the home directory of the project:
```
mvnw clean package
```
After a successful build the application is located in the target directory. From the
target directory the application can be started with the following command:
```
java -jar coding-dojo-spring-boot-1.0.jar --openweathermap.api-key=<your-API-key> --db.database-password=<db-password>
``` 
Replace `<your-API-key>` with the API key you have received when you registered at  
OpenWeatherMap.<br>
Replace `<db-password>` with the password you choose when you started the database.

## Using the application
When the application is started you can make a REST call to get the current weather in a 
specific city.
The URL of the application is `http://localhost:8080/` and to get the weather for a 
specific city run the following GET request:
```
curl 'http://localhost:8080/weather?city=Amsterdam'
``` 

